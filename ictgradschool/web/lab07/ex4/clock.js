var myVar = setInterval(myTimer, 1000);

function myTimer() {
    var d = new Date();
    document.getElementById("divClock").innerHTML = d.toLocaleTimeString();
}

document.getElementById("stop").addEventListener("click", myStop);

function myStop() {
    clearInterval(myVar);
}

function myResume(){
    myVar = setInterval(myTimer,1000);
}
document.getElementById("resume").addEventListener("click",myResume);