var x = document.getElementsByClassName("page");
for (let i = 0; i < x.length; i++) {
    x[i].addEventListener("click", function () {
        this.classList.add("pageAnimation");
        x[i].addEventListener("webkitAnimationEnd", function(){
            x[i+1].style.zIndex += 1;
        });
        x[i].addEventListener("animationend", function(){
            x[i+1].style.zIndex += 1;
        });
    });
}
