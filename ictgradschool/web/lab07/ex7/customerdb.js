var customers = [
    {"name": "Peter Jackson", "gender": "male", "year_born": 1961, "joined": "1997", "num_hires": 17000},

    {"name": "Jane Campion", "gender": "female", "year_born": 1954, "joined": "1980", "num_hires": 30000},

    {"name": "Roger Donaldson", "gender": "male", "year_born": 1945, "joined": "1980", "num_hires": 12000},

    {"name": "Temuera Morrison", "gender": "male", "year_born": 1960, "joined": "1995", "num_hires": 15500},

    {"name": "Russell Crowe", "gender": "male", "year_born": 1964, "joined": "1990", "num_hires": 10000},

    {"name": "Lucy Lawless", "gender": "female", "year_born": 1968, "joined": "1995", "num_hires": 5000},

    {"name": "Michael Hurst", "gender": "male", "year_born": 1957, "joined": "2000", "num_hires": 15000},

    {"name": "Andrew Niccol", "gender": "male", "year_born": 1964, "joined": "1997", "num_hires": 3500},

    {"name": "Kiri Te Kanawa", "gender": "female", "year_born": 1944, "joined": "1997", "num_hires": 500},

    {"name": "Lorde", "gender": "female", "year_born": 1996, "joined": "2010", "num_hires": 1000},

    {"name": "Scribe", "gender": "male", "year_born": 1979, "joined": "2000", "num_hires": 5000},

    {"name": "Kimbra", "gender": "female", "year_born": 1990, "joined": "2005", "num_hires": 7000},

    {"name": "Neil Finn", "gender": "male", "year_born": 1958, "joined": "1985", "num_hires": 6000},

    {"name": "Anika Moa", "gender": "female", "year_born": 1980, "joined": "2000", "num_hires": 700},

    {"name": "Bic Runga", "gender": "female", "year_born": 1976, "joined": "1995", "num_hires": 5000},

    {"name": "Ernest Rutherford", "gender": "male", "year_born": 1871, "joined": "1930", "num_hires": 4200},

    {"name": "Kate Sheppard", "gender": "female", "year_born": 1847, "joined": "1930", "num_hires": 1000},

    {"name": "Apirana Turupa Ngata", "gender": "male", "year_born": 1874, "joined": "1920", "num_hires": 3500},

    {"name": "Edmund Hillary", "gender": "male", "year_born": 1919, "joined": "1955", "num_hires": 10000},

    {"name": "Katherine Mansfield", "gender": "female", "year_born": 1888, "joined": "1920", "num_hires": 2000},

    {"name": "Margaret Mahy", "gender": "female", "year_born": 1936, "joined": "1985", "num_hires": 5000},

    {"name": "John Key", "gender": "male", "year_born": 1961, "joined": "1990", "num_hires": 20000},

    {"name": "Sonny Bill Williams", "gender": "male", "year_born": 1985, "joined": "1995", "num_hires": 15000},

    {"name": "Dan Carter", "gender": "male", "year_born": 1982, "joined": "1990", "num_hires": 20000},

    {"name": "Bernice Mene", "gender": "female", "year_born": 1975, "joined": "1990", "num_hires": 30000}
];


var myTable = document.getElementById("myTable");
var row = {};
var cell = {};
for (let i = 0; i < customers.length + 1; i++) {
    row[i] = myTable.insertRow(i);
}
var getMale = 0;
var getFemale = 0;
var get030 = 0;
var get3164 = 0;
var get65 = 0;
var getWeeks = 0.0;
var getGold = "";
var getSliver = "";
var getBronze = "";
var getDivide = 0.0;

//Making thead
var cell0 = row[0].insertCell(0);
var cell1 = row[0].insertCell(1);
var cell2 = row[0].insertCell(2);
var cell3 = row[0].insertCell(3);
var cell4 = row[0].insertCell(4);
var cell5 = row[0].insertCell(5);
cell0.textContent = "";
cell1.textContent = "Name";
cell2.textContent = "Gender";
cell3.textContent = "Year born";
cell4.textContent = "Joined year";
cell5.textContent = "Hires Number";

//Making tbody
for (let i = 1; i < customers.length + 1; i++) {
    cell[0] = row[i].insertCell(0);
    cell[0].textContent = i;
    for (let j = 1; j < 6; j++) {
        cell[j] = row[i].insertCell(j);
        switch (j) {
            case 1:
                cell[j].textContent = customers[i - 1].name;
                break;
            case 2:
                cell[j].textContent = customers[i - 1].gender;
                if (customers[i - 1].gender === "male") {
                    getMale++;
                }
                else {
                    getFemale++;
                }
                break;
            case 3:
                cell[j].textContent = customers[i - 1].year_born;
                let age = 2018 - customers[i - 1].year_born;
                if (age <= 30) {
                    get030++;
                }
                if (age > 30 && age < 65) {
                    get3164++;
                }
                if (age >= 65) {
                    get65++;
                }
                break;
            case 4:
                cell[j].textContent = customers[i - 1].joined;
                getWeeks = (2018.0 - customers[i - 1].joined) * 52;
                break;
            case 5:
                cell[j].textContent = customers[i - 1].num_hires;
                getDivide = customers[i - 1].num_hires / getWeeks;
                if (getDivide > 4) {
                    getGold += (customers[i - 1].name + ", ");
                }
                if (getDivide >= 1 && getDivide <= 4) {
                    getSliver += (customers[i - 1].name + ", ");
                }
                if (getDivide < 1) {
                    getBronze += (customers[i - 1].name + ", ");
                }
                break;
        }
    }
}

var myTfoot = document.getElementById("mySummary");
var row0 = myTfoot.insertRow(0);
var row1 = myTfoot.insertRow(1);
var row2 = myTfoot.insertRow(2);
var row3 = myTfoot.insertRow(3);
var row4 = myTfoot.insertRow(4);
var row5 = myTfoot.insertRow(5);
var row6 = myTfoot.insertRow(6);
var row7 = myTfoot.insertRow(7);

cell0 = row0.insertCell(0);
cell1 = row0.insertCell(1);
cell0.textContent = "Males";
cell1.textContent = getMale;
cell0 = row1.insertCell(0);
cell1 = row1.insertCell(1);
cell0.textContent = "Females";
cell1.textContent = getFemale;


cell0 = row2.insertCell(0);
cell1 = row2.insertCell(1);
cell0.textContent = "Age<31";
cell1.textContent = get030;
cell0 = row3.insertCell(0);
cell1 = row3.insertCell(1);
cell0.textContent = "30<Age<65";
cell1.textContent = get3164;
cell0 = row4.insertCell(0);
cell1 = row4.insertCell(1);
cell0.textContent = "64<Age";
cell1.textContent = get65;


cell0 = row5.insertCell(0);
cell1 = row5.insertCell(1);
cell0.textContent = "Gold";
cell1.textContent = getGold;
cell0 = row6.insertCell(0);
cell1 = row6.insertCell(1);
cell0.textContent = "Sliver";
cell1.textContent = getSliver;
cell0 = row7.insertCell(0);
cell1 = row7.insertCell(1);
cell0.textContent = "Bronze";
cell1.textContent = getBronze;
